<?php 
require_once 'Persistencia/Conexion.php';
require_once 'Persistencia/CursoDAO.php';
class Curso{
    private $id,$nombre,$creditos,$cursoDAO,$conexion;
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    /**
     * @return CursoDAO
     */
    public function getCursoDAO()
    {
        return $this->cursoDAO;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    public function Curso($id="",$nombre="",$creditos=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->creditos=$creditos;
        $this->cursoDAO=new CursoDAO($id,$nombre,$creditos);
        $this->conexion=new Conexion();        
    }
    public function insertar() {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> insertar());
        $this -> conexion -> cerrar();
        ;
    }
    public function consultarCursos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultarCursos());
        $Cursos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Curso($resultado[0], $resultado[1], $resultado[2]);
            array_push($Cursos, $c);
        }
        $this -> conexion -> cerrar();
        return $Cursos;
    }
    
}
?>