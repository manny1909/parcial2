<?php 
require_once 'Persistencia/Conexion.php';
require_once 'Persistencia/EstudianteDAO.php';
class Estudiante{
    private $id, $nombre, $apellido,$idCurso;
    private $EstudianteDAO, $conexion;
    
    public function Estudiante($id="",$nombre="",$apellido="",$idCurso=""){
        $this->id=$id;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->idCurso=$idCurso;        
        $this->EstudianteDAO=new EstudianteDAO($id,$nombre,$apellido,$idCurso);
        $this->conexion=new Conexion();
    }
    public function insertar() {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EstudianteDAO -> insertar());
        $this -> conexion -> cerrar();
        ;
    }
    public function consultarEstudiantes(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> EstudianteDAO -> consultarEstudiantes());
        $Estudiantes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $e = new Estudiante($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($Estudiantes, $e);
        }
        $this -> conexion -> cerrar();
        return $Estudiantes;
    }
   public function getId() {
       return $this->id;
   } 
   public function getNombre() {
       return $this->nombre;
   } 
   public function getApellido() {
       return $this->apellido;
   } 
   public function getIdCurso() {
       return $this->idCurso;
   } 
    
}
?>