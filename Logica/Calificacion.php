<?php 
require_once 'Persistencia/CalificacionDAO.php';
require_once 'Persistencia/Conexion.php';
class Calificacion{
    private $idCal,$descripcion,$nota,$idEstudiante,$calificacionDAO,$conexion;
    /**
     * @return string
     */
    public function getIdCal()
    {
        return $this->idCal;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return string
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * @return string
     */
    public function getIdEstudiante()
    {
        return $this->idEstudiante;
    }

    /**
     * @return CalificacionDAO
     */
    public function getCalificacionDAO()
    {
        return $this->calificacionDAO;
    }

    /**
     * @return Conexion
     */
    public function getConexion()
    {
        return $this->conexion;
    }

    public function Calificacion($idCal="",$descripcion="",$nota="",$idEstudiante="") {
        $this->idCal=$idCal;
        $this->descripcion=$descripcion;
        $this->nota=$nota;
        $this->idEstudiante=$idEstudiante;
        $this->calificacionDAO= new CalificacionDAO($descripcion,$nota,$idEstudiante);
        $this->conexion=new Conexion();
    }
    public function insertar() {
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> calificacionDAO -> insertar());
        $this -> conexion -> cerrar();
        
    }
    public function consultarCalificaciones(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> calificacionDAO -> consultarCalificaciones());
        $Calificaciones = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Calificacion($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($Calificaciones, $c);
        }
        $this -> conexion -> cerrar();
        return $Calificaciones;
    }
    public function notaPromedio() {
        $calificaciones= consultarCalificaciones();
        $aux=0;
        if($calificaciones!=null){
            for ($i = 0; $i < count($calificaciones); $i++) {
               $aux+=$calificaciones[2]; 
            }
        }
    }
    
}
?>