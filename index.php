<?php 
require_once 'Logica/Curso.php';
require_once 'Logica/Estudiante.php';
require_once 'Logica/Calificacion.php';
$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);
}
?>
<head>
	<link rel="icon" type="image/png" href="" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	
</head>
<?php 
include "Presentacion/encabezado.php";
if($pid!=""){
    include $pid;
}

?>
