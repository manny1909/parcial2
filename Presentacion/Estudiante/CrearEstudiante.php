<?php
$nombre="";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$apellido = "";
if(isset($_POST["apellido"])){
    $apellido = $_POST["apellido"];
}
$idCurso = "";
if(isset($_POST["idCurso"])){
    $idCurso = $_POST["idCurso"];
}    


if(isset($_POST["crear"])){
    $Estudiante = new Estudiante("", $nombre, $apellido, $idCurso);
    $Estudiante -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Estudiante</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Estudiante registrado
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Estudiante/CrearEstudiante.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label> 
							<input type="text" name="apellido" class="form-control" min="1" value="<?php echo $apellido ?>" required>
						</div>
						
						<?php 
						require_once "Persistencia/Conexion.php";						
						$cont; $conexion=new Conexion();
						$conexion->abrir();
						$conexion->ejecutar("select *from curso");
						$aux="";
						for ($i = 0; $i < $conexion->numFilas(); $i++) {
						    $resultado=$conexion->extraer();
						    $value=$resultado[0];
						    $nProv=$resultado[0];
						    $aux.='<option value="'.$value.'">'.$nProv.'</option>';
						}
						$conexion->cerrar();
						echo ' <select class="custom-select" id="inputGroupSelect01" name="idCurso" >
                                    <option selected>Curso.</option>'.
                                    $aux.' </select>';
						?> 
						<button type="submit" name="crear" class="btn btn-info mt-3">crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>