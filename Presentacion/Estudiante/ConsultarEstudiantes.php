<?php
$estudiante= new Estudiante();
$estudiantes = $estudiante -> consultarEstudiantes();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Lista de estudiantes</h4>
				</div>
				<div class="text-right"><?php echo count($estudiantes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>id Estudiante</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>ID Curso</th>
							<th>Calificaciones </th>
						</tr>
						<?php 
						$i=1;
						foreach($estudiantes as $estudianteActual){
						    echo "<tr>";
						    echo "<td>" . $estudianteActual -> getId() . "</td>";
						    echo "<td>" . $estudianteActual -> getNombre() . "</td>";
						    echo "<td>" . $estudianteActual -> getApellido() . "</td>";
						    echo "<td>" . $estudianteActual -> getIdCurso() . "</td>";
						    $pid=base64_encode("Presentacion/Calificaciones/Calificaciones.php");
						    echo '<td> <a href="index.php?pid='.$pid.'&idEstudiante='.$estudianteActual->getId().'">ver Calificaciones</a></td>';						    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
						
					</table> 
				</div>
            </div>
		</div>
	</div>
</div>