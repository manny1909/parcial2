<?php
$descripcion = "";
if(isset($_POST["descripcion"])){
    $descripcion = $_POST["descripcion"];
}
$nota = "";
if(isset($_POST["nota"])){
    $nota = $_POST["nota"];
}
$idEstudiante="";
if(isset($_GET["idEstudiante"])){
    $idEstudiante=$_GET["idEstudiante"];
}
if(isset($_POST["crear"])){
    $calificacion = new Calificacion("", $descripcion, $nota,$idEstudiante);
    $calificacion -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Registrar nota</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Nota registrada
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Calificaciones/Calificaciones.php") ?>&idEstudiante=<?php echo $idEstudiante?>" method="post">
						<div class="form-group">
							<label>Descripcion</label> 
							<input type="text" name="descripcion" class="form-control" value="<?php echo $descripcion ?>" required>
						</div>
						<div class="form-group">
							<label>Nota</label> 
							<input type="number" name="nota" class="form-control" min="1" value="<?php echo $nota ?>" required>
						</div>
						
						<button type="submit" name="crear" class="btn btn-info">Registrar Nota</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
<?php
$calificacion = new Calificacion("", $descripcion, $nota,$idEstudiante);
$calificaciones= $calificacion -> consultarCalificaciones();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Lista de calificaciones</h4>
				</div>
				<div class="text-right"><?php echo count($calificaciones) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>id calificacion</th>
							<th>descripcion</th>
							<th>nota</th>
						</tr>
						<?php 
						$i=1;
						foreach($calificaciones as $calificacionActual){
						    echo "<tr>";
						    echo "<td>" . $calificacionActual -> getIdCal() . "</td>";
						    echo "<td>" . $calificacionActual -> getDescripcion() . "</td>";
						    echo "<td>" . $calificacionActual -> getNota() . "</td>";					    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
						
					</table> 
				</div>
            </div>
		</div>
	</div>
</div>