<nav class="navbar navbar-expand-lg navbar-light bg-info" >
  <a class="navbar-brand" href="index.php"><b>Sistema Academico</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav"> 
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Estudiante
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="index.php?pid=<?php echo  base64_encode("Presentacion/Estudiante/CrearEstudiante.php")?>">Crear estudiante</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo  base64_encode("Presentacion/Estudiante/ConsultarEstudiantes.php")?>">Consultar estudiantes</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Curso
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="index.php?pid=<?php echo  base64_encode("Presentacion/Curso/CrearCurso.php")?>">Crear curso</a>
          <a class="dropdown-item" href="index.php?pid=<?php echo  base64_encode("Presentacion/Curso/ConsultarCursos.php")?>">Consultar curso</a>
        </div>
      </li>      
    </ul>
  </div>
</nav>