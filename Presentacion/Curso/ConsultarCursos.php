<?php
$curso= new Curso();
$cursos = $curso -> consultarCursos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>CLista de cursos</h4>
				</div>
				<div class="text-right"><?php echo count($cursos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>id Curso</th>
							<th>Nombre</th>
							<th>Creditos</th>
						</tr>
						<?php 
						$i=1;
						foreach($cursos as $cursoActual){
						    echo "<tr>";
						    echo "<td>" . $cursoActual -> getId() . "</td>";
						    echo "<td>" . $cursoActual -> getNombre() . "</td>";
						    echo '<td>' . $cursoActual -> getCreditos() . '</td>';
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>