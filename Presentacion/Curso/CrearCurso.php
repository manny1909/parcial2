<?php
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$creditos = "";
if(isset($_POST["creditos"])){
    $creditos = $_POST["creditos"];
}
  
if(isset($_POST["crear"])){
    $curso = new Curso("", $nombre, $creditos);
    $curso -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Curso</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						curso creado
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Presentacion/Curso/CrearCurso.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>creditos del curso</label> 
							<input type="number" name="creditos" class="form-control" min="1" value="<?php echo $creditos ?>" required>
						</div>
						
						<button type="submit" name="crear" class="btn btn-info">Crear curso</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>